# README

---

### Database Structure

#### User Table

Field | Type | Description
:---  |:---: | :---:
id | int | primary key
userName | varchar(20) | must be unique
password | varchar(63) | MD5 algorithm used
gender | int(1) | 0-unknown，1-male，2-female
height | double | 
weight | double | 

#### Running Table

Field | Type | Description
:---  |:---: | :---:
id | int | primary key
userId | int(8)) | id in User Table
date | date | 
distance | double | run distance
time | double | in second
cal | double | calories burned
altitude | double | 
latitude | double | 
longitude | double | 
perdistance | double | 

---

### API

#### add a new user

	request	:	POST /api/user/add
	params	:	{username : between 1 and 20 characters
				 password: between 6 and 20 characters
				 height: float 
				 weight: float 
				 gender: 0-unknown，1-male，2-female}
	success	:	{code : 200 , msg : OK}
	error	:	{code : 403 , msg : Forbidden}
	
#### user login

	request	:	POST /api/user/login
	params	:	{username : between 1 and 20 characters, password : between 6 and 20 characters}
	success	:	{code : 200 , msg : OK, data : {User object}, token : token}
	error	:	{code : 403 , msg : Forbidden}
				{code : 404 , msg : Not Found}
				
#### user update physiological data

	request	:	POST /api/user/update
	params	:	{height : float, weight : float, gender: int}
	success	:	{code : 200 , msg : OK, data : {User object}}
	error	:	{code : 403 , msg : Forbidden}
				{code : 404 , msg : Not Found}
				
#### upload run data

	request	:	POST /api/running/add
	params	:	{distance : unsigned_float,
				 time : unsigned_float,
				 cal : unsigned_float,
				 altitude : float,
				 latitude : float,
				 longtitude : float,
				 perdistance : float
				 }
	success	:	{code : 200 , msg : OK, data : {User object}}
	error	:	{code : 403 , msg : Forbidden}
				{code : 404 , msg : Not Found}
				
#### retrieve run history

	request	:	GET /api/running/getAll
	success	:	{code : 200 , msg : OK, data : [{Running object},{Running object},..]}
	error	:	{code : 403 , msg : Forbidden}