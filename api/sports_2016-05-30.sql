# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 207.226.142.184 (MySQL 5.5.47-0ubuntu0.14.04.1)
# Database: sports
# Generation Time: 2016-05-30 10:25:38 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Running
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Running`;

CREATE TABLE `Running` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(8) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `time` double DEFAULT NULL,
  `cal` double DEFAULT NULL,
  `location` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Running` WRITE;
/*!40000 ALTER TABLE `Running` DISABLE KEYS */;

INSERT INTO `Running` (`id`, `userId`, `date`, `distance`, `time`, `cal`, `location`)
VALUES
	(1,1,'2016-05-30',4000,900,2,'10-10-10-10,11-11-11-11,12-12-12-12'),
	(2,1,'2016-05-30',4000,900,2,'10-10-10-10,11-11-11-11,12-12-12-12');

/*!40000 ALTER TABLE `Running` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table User
# ------------------------------------------------------------

DROP TABLE IF EXISTS `User`;

CREATE TABLE `User` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(63) DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `height` double DEFAULT NULL,
  `weight` double DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;

INSERT INTO `User` (`id`, `username`, `password`, `gender`, `height`, `weight`)
VALUES
	(1,'stone','e10adc3949ba59abbe56e057f20f883e',1,100,100),
	(2,'stone2','e10adc3949ba59abbe56e057f20f883e',1,173,55),
	(3,'stone3','e10adc3949ba59abbe56e057f20f883e',1,173,55),
	(4,'stone5','e10adc3949ba59abbe56e057f20f883e',1,120,120);

/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
