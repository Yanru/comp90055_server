var express = require('express'),
	expressValidator = require('express-validator'),
	util = require('util'),
	md5 = require('md5'),
	jwt = require('jsonwebtoken'),
	config = require('./config'),
	database = require('./database');

var router = express.Router();

// api - add a new user
router.post('/api/user/add', function(req, res, next) {
	// post body validation
	req.checkBody('username', 'Invalid username').isAlphanumeric().len(1, 20);
	req.checkBody('password', 'Invalid password').isAlphanumeric().len(6, 20);
	req.checkBody('height', 'Invalid height').isFloat();
	req.checkBody('weight', 'Invalid weight').isFloat();
	req.checkBody('gender', 'Invalid gender').isInt({ min: 0, max: 2 });
	var err = req.validationErrors();
	if (err) {
		res.status(403);
		res.json({ code: 403, msg: util.inspect(err) });
	} else {
		// check if username is unique
		database.getUserByName(req.body.username, function(err, results) {
			if (err) {
				next(err);
			} else if (results.length > 0) {
				res.status(403);
				res.json({ code: 403, msg: 'Username already exists.' });
			} else {
				var params = {
					username: req.body.username,
					password: md5(req.body.password),
					height: parseFloat(req.body.height),
					weight: parseFloat(req.body.weight),
					gender: parseInt(req.body.gender)
				};
				database.addUser(params, function(err, result) {
					if (err) {
						next(err);
					} else {
						database.getUserByLogin(req.body.username, md5(req.body.password), function(err, results) {
							if (err) {
								next(err);
							} else if (results.length == 0) {
								res.status(404);
								res.json({ code: 404, msg: 'Register failed.' });
							} else {
								delete results[0].password;
								var token = jwt.sign(results[0], config.token_secret, {expiresIn: config.token_expire});
								return res.json({ code: 200, msg: 'Login success.', token: token, data: results[0] });
							}
						});
					}
				});
			}
		});
	}
});

// api - user login
router.post('/api/user/login', function(req, res) {
	req.checkBody('username', 'Invalid username').isAlphanumeric().len(1, 20);
	req.checkBody('password', 'Invalid password').isAlphanumeric().len(6, 20);
	var err = req.validationErrors();
	if (err) {
		res.status(403);
		res.json({ code: 403, msg: util.inspect(err) });
	} else {
		database.getUserByLogin(req.body.username, md5(req.body.password), function(err, results) {
			if (err) {
				next(err);
			} else if (results.length == 0) {
				res.status(404);
				res.json({ code: 404, msg: 'Login failed.' });
			} else {
				delete results[0].password;
				var token = jwt.sign(results[0], config.token_secret, {expiresIn: config.token_expire});
				return res.json({ code: 200, msg: 'Login success.', token: token, data: results[0] });
			}
		});
	}
});

// api - verify token
router.use('/api', function(req, res, next) {
	var token = req.body.token || req.query.token || req.headers['x-access-token'];
	if (token) {
		jwt.verify(token, config.token_secret, function(err, decoded) {
    	  	if (err) {
				res.status(403);
	        	return res.json({ code: 403, msg: err.message });
      		} else {
      			req.decoded = decoded;
      			if (req.decoded.id) {
      				database.getUserById(req.decoded.id, function(err, results) {
      					if (err) {
							next(err);
						} else if (results.length == 0) {
							res.status(403);
							res.json({ code: 403, msg: 'Invalid token.' });
						} else {
	        				next();
						}
      				});
      			} else {
      				res.status(403);
					res.json({ code: 403, msg: 'Invalid token.' });
      			}
      		}
    	});
	} else {
		res.status(403);
		res.json({ code: 403, msg: 'No token provided.' });
	}
});

// api - update user's physiological data
router.post('/api/user/update', function(req, res, next) {
	req.checkBody('weight', 'Invalid weight').isFloat();
	req.checkBody('height', 'Invalid height').isFloat();
	req.checkBody('gender', 'Invalid gender').isInt({ min: 0, max: 2 });
	var err = req.validationErrors();
	if (err) {
		res.status(403);
		res.json({ code: 403, msg: util.inspect(err) });
	} else {
		var params = {
			height: parseFloat(req.body.height),
			weight: parseFloat(req.body.weight),
			gender: parseInt(req.body.weight),
			userId: req.decoded.id
		};
		database.updateUser(params, function(err, result) {
			if (err) {
				next(err);
			} else {
				res.json({ code: 200, msg: 'Update success.' });
			}
		});
	}
});

// api - retrieve user's run history
router.get('/api/running/getAll', function(req, res, next) {
	database.getAllRunningByUserId(req.decoded.id, function(err, results) {
		if (err) {
			next(err);
		} else {
			for (var i=0;i<results.length;i++) {
				var locations = [];
				var tmp = results[i].location;
				var locations_array = results[i].location.split(',');
				for (var j=0;j<locations_array.length;j++) {
					var datas = locations_array[j].split('@');
					if (datas.length >= 4) {
						var location = {
							altitude: datas[0],
							latitude: datas[1],
							longitude: datas[2],
							perdistance: datas[3]
						};
						locations.push(location);
					}
				}
				results[i].location = locations;
			}
			res.json({ code: 200, msg: 'Get running success.', data: results });
		}
	});
});

// api - upload run data
router.post('/api/running/add', function(req, res, next) {
	// post body validation
	req.checkBody('distance', 'Invalid distance').isFloat({ min: 0, max: 9999999});
	req.checkBody('time', 'Invalid time').isFloat({ min: 0, max: 9999999});
	req.checkBody('cal', 'Invalid cal').isFloat({ min: 0, max: 9999999});

	var location = [];
	if (req.body.location instanceof Array) {
		for (var i=0;i<req.body.location.length;i++) {
			var tmp = req.body.location[i];
			if (tmp.hasOwnProperty('altitude') && !isNaN(parseFloat(tmp['altitude'])) &&
				tmp.hasOwnProperty('latitude') && !isNaN(parseFloat(tmp['latitude'])) &&
				tmp.hasOwnProperty('longitude') && !isNaN(parseFloat(tmp['longitude'])) &&
				tmp.hasOwnProperty('perdistance') && !isNaN(parseFloat(tmp['perdistance']))) {
				location.push(tmp['altitude']+'@'+tmp['latitude']+'@'+tmp['longitude']+'@'+tmp['perdistance']);
			} else {
				res.status(403);
				return res.json({ code: 403, msg: 'Invalid location.' });
			}
		}
	} else {
		res.status(403);
		return res.json({ code: 403, msg: 'Invalid location.' });
	}

	var err = req.validationErrors();
	if (err) {
		res.status(403);
		res.json({ code: 403, msg: util.inspect(err) });
	} else {
		var params = {
			userId: req.decoded.id,
			date: new Date(),
			distance: parseFloat(req.body.distance),
			time: parseFloat(req.body.time),
			cal: parseFloat(req.body.cal),
			date: new Date(),
			location: location.join(',')
		}
		database.addRunning(params, function(err, results) {
			if (err) {
				next(err);
			} else if (results.length == 0) {
				res.status(404);
				res.json({ code: 404, msg: 'Add running failed.' });
			} else {
				res.json({ code: 200, msg: 'Add running success.' });
			}
		});
	}
});

module.exports = router;