var mysql      = require('mysql');
var connection = mysql.createPool({
	connectionLimit	: 1000,
	host     		: '127.0.0.1',
	user     		: 'root',
	password 		: 'admin123',
	database 		: 'sports'
});

exports.addUser = function(params, callback) {
	connection.getConnection(function(err, connection) {
		if (err) return callback(err);
		var sql = 'insert into User set ?';
		connection.query(sql, params, function(err, result) {
			connection.release();
			callback(err, result);
		});
	});
};

exports.getUserByName = function(username, callback) {
	connection.getConnection(function(err, connection) {
		if (err) return callback(err);
		var sql = 'select * from User where username="'+username+'"';
		connection.query(sql, function(err, results) {
			connection.release();
			callback(err, results);
		});
	});
};

exports.getUserById = function(id, callback) {
	connection.getConnection(function(err, connection) {
		if (err) return callback(err);
		var sql = 'select * from User where id="'+id+'"';
		connection.query(sql, function(err, results) {
			connection.release();
			callback(err, results);
		});
	});
};

exports.updateUser = function(params, callback) {
	connection.getConnection(function(err, connection) {
		if (err) return callback(err);
		var sql = 'update User set height='+params.height+', weight='+params.weight+ ', gender='+params.gender+' where id='+params.userId;
		connection.query(sql, function(err, result) {
			connection.release();
			callback(err, result);
		});
	});
};

exports.getUserByLogin = function(username, md5_password, callback) {
	connection.getConnection(function(err, connection) {
		if (err) return callback(err);
		var sql = 'select * from User where username="'+username+'" and password="'+md5_password+'"';
		connection.query(sql, function(err, results) {
			connection.release();
			callback(err, results);
		});
	});	
};

exports.addRunning = function(params, callback) {
	connection.getConnection(function(err, connection) {
		if (err) return callback(err);
		var sql = 'insert into Running set ?';
		connection.query(sql, params, function(err, result) {
			connection.release();
			callback(err, result);
		});
	});
};

exports.getAllRunningByUserId = function(userId, callback) {
	connection.getConnection(function(err, connection) {
		if (err) return callback(err);
		var sql = 'select * from Running where userId="'+userId+'"';
		connection.query(sql, function(err, results) {
			connection.release();
			callback(err, results);
		});
	});
};