var express = require('express'),
    expressValidator = require('express-validator'),
    logger = require('morgan'),
    bodyParser = require('body-parser');

var api = require('./api'),
    config = require('./config');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator([]));

app.use('/', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    console.log(err);
    res.status(err.status || 500);
    res.json({
        code: err.status || 500,
        msg: err.message
    });
});

// start server
if (!module.parent) {
    var port = Number(process.env.PORT || config.port);
    var server = app.listen(port, function () {
        console.log('-----------------------------------------------------');
        console.log(config.name);
        console.log("Express server listening on port %s:%d in %s mode", server.address().address, port, app.settings.env);
        console.log('-----------------------------------------------------');
    });
}

module.exports = app;